#Author: sapalac@bancolombia.com.co
Feature: Add two numbers
  I want to sum two numbers
  
  @manual
  Scenario: Add two numbers manual
    Given open the calculator
    When I add 4 and 8
    Then the result should be 12

  @manual
  Scenario: Add two numbers manual
    Given open the calculator
    When I add 4 and 8
    Then the result should be 32
