#Author: sapalac@bancolombia.com.co
Feature: User Story 722240: DT2 - APM - HU - 327 Crear los nuevos campos en la pantalla de entrada, para las secciones: Libranza Pensionados y Observaciones
	Criterios de aceptación:
  * Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo, puntualmente, incluir los nuevos campos en la secciones: Libranza Pensionados y Observaciones
	* Realización de pruebas en certificación.

  @seccion_libranza_pensionado @327
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de LIBRANZA PENSIONADO
    Then visualiza una nueva sección de Libranza Pensionados que se muestre cuando se seleccione Aplica libranza en Si y Ocupación igual a Pensionado
	And se adicione el campo Seguro Vida en la sección Libranza Pensionados
	And el campo Seguro Vida sea opcional
	And se muestre el campo DTF en la sección Libranza Pensionados
	And el campo DTF sea opcional
	And se muestre el campo Tasa Mes Vencido en la sección Libranza Pensionados
	And el campo Tasa Mes Vencido sea opcional

    
 
	@seccion_observaciones @327
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de OBSERVACIONES
    Then visualiza una nueva sección de Observaciones cuando se seleccione Aplica libranza igual a Si
	And se muestre el campo Caso Bizagi en la sección Observaciones cuando se seleccione Aplica libranza igual a Si de tipo texto
	And el campo Caso Bizagi sea opcional
	And se muestre el campo Observaciones Comercial en la sección Observaciones cuando se seleccione Aplica libranza igual a Si de tipo texto
	And el campo Observaciones Comercial sea opcional
	And se muestre el campo Solicitud Multiproducto en la sección Observaciones cuando se seleccione Aplica libranza igual a Si de tipo texto
	And el campo Solicitud Multiproducto sea opcional


