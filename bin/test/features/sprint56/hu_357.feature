#Author: sapalac@bancolombia.com.co
Feature: User Story 740998: DT2 - APM - HU - 357 Creación de campos en la estructura y entidades
	Criterios de aceptación:
  * Evidencia de la creación de los nuevos campos en la estructura y entidades correspondientes, necesarios en APM para implementar 
  las modificaciones en la eestructuración de la pantalla de entrada de Consumo


  @PRINCIPAL @Sprint56
  Scenario: Evidencia de la creación de los nuevos campos en la estructura y entidades correspondientes, necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se accede a la BD de APM
    When se consulte la tabla OSUSR_cqq_PersonAp con NAME igual a PersonApplicant
    Then se visualicen los nuevos campos adicionados ESTRATO y SEMESTRE
	And que al ingresar a la BD de APM se visualice la nueva tabla para los campos de libranza
	
  @LIBRANZA @Sprint56
  Scenario: Evidencia de la creación de los nuevos campos en la estructura y entidades correspondientes, necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se accede a la BD de APM
    When se consulte la tabla OSUSR_cqq_Applic13
    Then que la tabla contenga el campo ID
	And que la tabla contenga el campo APPLICATIONID
	And que la tabla contenga el campo APPLYLIBRANZA
	And que la tabla contenga el campo NUMBEROFMANDATORY
	And que la tabla contenga el campo CODEOFCONTRACT
	And que la tabla contenga el campo REQUESTTERM
	And que la tabla contenga el campo PURCHASEOFPORTFOLIO
	And que la tabla contenga el campo COMPANYTYPE
	And que la tabla contenga el campo QUALIFICATIONCOMPANY
	And que la tabla contenga el campo SEGMENTCOMPANY
	And que la tabla contenga el campo INSUREUNEMPLOYED
	And que la tabla contenga el campo INSURELIFEMORE
	And que la tabla contenga el campo LIFEINSURANCE
	And que la tabla contenga el campo DTF
	And que la tabla contenga el campo RATEOFMONTHEXPIRED
	And que la tabla contenga el campo BIZAGICASE
	And que la tabla contenga el campo COMERCIALOBSERVATIONS
	And que la tabla contenga el campo MULTIPRODUCTREQUEST
	And que la tabla contenga el campo DATEENTAILMENTCOMPANY
	And que la tabla contenga el campo DEDUCTIONSTOTALS
	And que la tabla contenga el campo DEDUCTIONSPERCENTAGE
    