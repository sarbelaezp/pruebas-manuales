#Author: sapalac@bancolombia.com.co
Feature: User Story 740993: DT2 - APM - HU - 354 Modificación de variables existentes en la pantalla de entrada de APM
	Criterios de aceptación:
  * Realizar las modificaciones en las variables existentes para implementar las modificaciones 
  	en la restructuración de la pantalla de entrada de Consumo


  @PRODUCTO_PLAZO @Sprint56
  Scenario: Realizar las modificaciones en las variables existentes para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria seleccione un producto de la lista se visualice el campo PLAZO SOLICITADO con el plazo que se haya parametrizado para el producto
    Then que se haga la validación para que el plazo máximo para CREDIÁGIL -P04 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para CREDITO EJECUTIVO EMPRESARIAL - P06 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para COMPRA DE CARTERA LIBRE INVERSION - P79 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para RETANQUEO L INVERSION - P80 -P48 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para RETANQUEO L INVERSION - D78 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para CR PERSONAL CON DTF - D12 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para PIGNORACION PENSIONES BULLET - P36 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para COMPRA DE CARTERA CONSUMO VARIABLE - D20 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para PIGNORACION PENSIONES T.FIJA - P22 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para PERSONAL TASA FIJA S.DESEMPLEO- P48 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para PIGNORACIÓN DE PENSIONES T. VARIAB - D22 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para PERSONAL VARIABLE CUOTA FIJA - D78 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para CREDITO DE LIBRE INVERSION TASA FIJA - P12 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para AMEX PLATINUM sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para AMEX GREEN sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para AMEX BLUE sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para AMEX GOLD sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC BLACK sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC CLASICA sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC ECARD sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC IDEAL sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC SUFI ORO sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC ORO sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC PLATINUM sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC JOVEN sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC JOVEN UNIVERSITARIOS sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para MC AFINIDAD sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para PLATINUM sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para CAYMAN ORO sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para VISA LIFEMILES sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para VISA ORO sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para VISA SELECCIÓN sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para VISA CLÁSICA sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para VISA INFINITY sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para VISA AFINIDAD sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA TASA FIJA - P25 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA BANCO - T58 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA EMPL. FILIALES - P55 sea menor o igual a 60 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA CON DTF - D09 sea menor o igual a 96 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA COMPRA CARTERA DTF -D04 sea menor o igual a 96 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA EMPLEADOS BANCO DTF -T36 sea menor o igual a 96 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA EMPLEADOS FILIAL DTF - P88 sea menor o igual a 96 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para RETANQUEO LIBRANZA DTF -D07 sea menor o igual a 96 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA FOPEP -D14 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para COMPRA DE CARTERA FOPEP-D21 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA PENSIONADO PROTECCION -P67 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA COLPENSIONES - D79 sea menor o igual a 120 y mayor o igual a 36
	And que se haga la validación para que el plazo máximo para LIBRANZA COMPRA COLPENSIONES -D80 sea menor o igual a 120 y mayor o igual a 36
	And que cuando se diligencie un plazo por debajo de 36 se muestre el mensaje: “El plazo diligenciado está por debajo del mínimo permitido para el producto”
	And que se haga la validación para que el PORCENTAJE DEDUCCIONES permita únicamente valores entre 20 y 50

	
  @CALIFICACION_INTERNA_EMPRESA @Sprint56
  Scenario: Realizar las modificaciones en las variables existentes para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria intente seleccionar la lista CALIFICACIÓN INTERNA EMPRESA
    Then que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R1
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R2
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R3 	
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R4 	
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R5 	
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R6 	
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R7
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R8 	
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R9	
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R10
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO contenga R11

	
	@SEMESTRE @Sprint56
  Scenario: Realizar las modificaciones en las variables existentes para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria intente seleccionar la lista SEMESTRE
    Then que el campo SEMESTRE contenga el valor 1
	And que el campo SEMESTRE contenga el valor 2
	And que el campo SEMESTRE contenga el valor 3
	And que el campo SEMESTRE contenga el valor 4
	And que el campo SEMESTRE contenga el valor 5
	And que el campo SEMESTRE contenga el valor 6
	And que el campo SEMESTRE contenga el valor 7
	And que el campo SEMESTRE contenga el valor 8
	And que el campo SEMESTRE contenga el valor 9
	And que el campo SEMESTRE contenga el valor 10
	And que el campo SEMESTRE contenga el valor 11
	And que el campo SEMESTRE contenga el valor 12
    
    
    @SEGMENTO_EMPRESA_CONVENIO @Sprint56
  Scenario: Realizar las modificaciones en las variables existentes para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria intente seleccionar la lista SEGMENTO EMPRESA - CONVENIO
    Then que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor CONSTRUCTOR CORPORATIVO
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor CONSTRUCTOR EMPRESARIAL
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor CONSTRUCTOR PYME
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor CORPORATIVO
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor EMPRESARIAL
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor INSTITUCIONES FINANCIERAS
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor GOBIERNO
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor GOBIERNO DE RED
	And que el campo SEGMENTO EMPRESA - CONVENIO contenga el valor PYMES
    
    
    @ESTRATO @Sprint56
  Scenario: Realizar las modificaciones en las variables existentes para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria intente seleccionar la lista ESTRATO
    Then que el campo ESTRATO contenga el valor 1
	And que el campo ESTRATO contenga el valor 2
	And que el campo ESTRATO contenga el valor 3
	And que el campo ESTRATO contenga el valor 4
	And que el campo ESTRATO contenga el valor 5
	And que el campo ESTRATO contenga el valor 6
    