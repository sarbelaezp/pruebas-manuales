#Author: sapalac@bancolombia.com.co
Feature: User Story 740994: DT2 - APM - HU - 356 Validación de campos numéricos y tipo texto, obligatoriedad y dependencia con otros campos
	Criterios de aceptación:
  * Desarrollos correspondientes en los campos numéricos y de tipo texto, considerando la obligatoriedad y dependencia entre sí de los campos en la pantalla de entrada de APM en Consumo


  @OBLIGATORIEDAD @Sprint56
  Scenario: Desarrollos correspondientes en los campos numéricos y de tipo texto, considerando la obligatoriedad y dependencia entre sí de los campos en la pantalla de entrada de APM en Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria intente procesar una solicitud
    Then que el campo NÚMERO DOCUMENTO reciba valores de tipo ALFANÚMERICA
	And que el campo CASO BIZAGI reciba valores de tipo ALFANÚMERICA
	And que el campo OBSERVACIONES COMERCIAL reciba valores de tipo ALFANÚMERICA
	And que el campo SOLICITUD MULTIPRODUCTO reciba valores de tipo ALFANÚMERICA
	And que el campo VALOR SOLICITADO reciba valores >=0 de tipo ENTERO
	And que el campo PLAZO SOLICITADO (meses) reciba valores >=36 y <=120 de tipo ENTERO
	And que el campo TIEMPO ACTIVIDAD (MESES) reciba valores >=0 de tipo ENTERO
	And que el campo INGRESOS FIJOS reciba valores >=0 de tipo ENTERO
	And que el campo INGRESOS VARIABLES reciba valores >=0 de tipo ENTERO
	And que el campo INGRESOS COMO INDEPENDIENTE reciba valores >=0 de tipo ENTERO
	And que el campo OTROS INGRESOS/RENTAS/HONORARIOS/GIROS reciba valores >=0 de tipo ENTERO
	And que el campo GASTO FAMILIAR reciba valores >=0 de tipo ENTERO
	And que el campo CUOTAS CANCELADAS reciba valores >=0 de tipo ENTERO
	And que el campo CUPOS TDC CANCELADAS reciba valores >=0 de tipo ENTERO
	And que el campo CUPOS ROTATIVOS CANCELADOS reciba valores >=0 de tipo ENTERO
	And que el campo CUOTAS RECIENTES reciba valores >=0 de tipo ENTERO
	And que el campo CUPOS TDC Y ROTATIVOS RECIENTES reciba valores >=0 de tipo ENTERO
	And que el campo CUOTA CRÉDITO A COMPRAR reciba valores >=0 de tipo ENTERO
	And que el campo CUOTA CRÉDITO A COMPRAR COLILLA reciba valores >=0 de tipo ENTERO
	And que el campo VALOR TOTAL DE LOS ACTIVOS reciba valores >=0 de tipo ENTERO
	And que el campo CÓDIGO CONVENIO reciba valores DIFERENTE DE CERO de tipo ENTERO
	And que el campo DEDUCCIONES TOTALES reciba valores >=0 de tipo ENTERO
	And que el campo PORCENTAJE DEDUCCIONES reciba valores >=20 y <=50 de tipo ENTERO
	And que el campo SEGURO DE VIDA reciba valores >=0 de tipo ENTERO
	And que el campo DTF reciba valores de tipo DECIMAL
	And que el campo TASA MES VENCIDO reciba valores de tipo DECIMAL
	And que el campo PLAZO SOLICITADO sea obligatorio Si
	And que el campo ESTRATO sea obligatorio No
	And que el campo SEMESTRE sea obligatorio No
	And que el campo CUOTA CRÉDITO A COMPRAR sea obligatorio No
	And que el campo CUOTA CRÉDITO A COMPRAR COLILLA  sea obligatorio No
	And que el campo COMPRA DE CARTERA Y RETANQUEO sea obligatorio No
	And que el campo FORMATO CONOCIMIENTO DEL CLIENTE sea obligatorio Si
	And que el campo APLICA LIBRANZA sea obligatorio Si
	And que el campo CÓDIGO CONVENIO sea obligatorio No
	And que el campo TIPO DE EMPRESA sea obligatorio No
	And que el campo CALIFICACIÓN INTERNA EMPRESA - CONVENIO sea obligatorio No
	And que el campo SEGMENTO EMPRESA - CONVENIO sea obligatorio No
	And que el campo FECHA VINCULACIÓN EMPRESA sea obligatorio No
	And que el campo DEDUCCIONES TOTALES sea obligatorio No
	And que el campo PORCENTAJE DEDUCCIONES sea obligatorio No
	And que el campo SEGURO DESEMPLEO sea obligatorio No
	And que el campo SEGURO VIDA MAS sea obligatorio No
	And que el campo  SEGURO DE VIDA sea obligatorio No
	And que el campo DTF sea obligatorio No
	And que el campo TASA MES VENCIDO sea obligatorio No
	And que el campo CASO BIZAGI sea obligatorio No
	And que el campo OBSERVACIONES COMERCIAL sea obligatorio No
	And que el campo SOLICITUD MULTIPRODUCTO sea obligatorio No
