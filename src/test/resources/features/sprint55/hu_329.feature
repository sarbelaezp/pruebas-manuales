#Author: sapalac@bancolombia.com.co
Feature: User Story 722243: DT2 - APM - HU - 329 Ocultar variables de las secciones que no correspondan al seleccionar en la variable Aplica Libranza No
	Criterios de aceptación:
  * Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo, puntualmente, para ocultar en la pantalla de entrada las secciones que no correspondan al seleccionar en la variable Aplica Libranza No.
	* Realización de pruebas en certificación.

  @seccion_solicitud @329
  Scenario: Ocultar en la pantalla de entrada las secciones que no correspondan al seleccionar en la variable Aplica Libranza No.
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de SOLICITUD
    Then al seleccionar Aplica Libranza igual a No, no se visualice el campo Compra de cartera y Retanqueo
	And al seleccionar Aplica Libranza igual a No, no se visualice el campo Codigo de convenio