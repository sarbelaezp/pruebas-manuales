#Author: sapalac@bancolombia.com.co
Feature: User Story 722242: DT2 - APM - HU - 328 Ocultar variables en la sección Financiera para Cupos y Cuotas/ Recientes/Canceladas/a Comprar
	Criterios de aceptación:
  * Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo, puntualmente, para ocultar en la pantalla de entrada las variables correspondientes a la sección Financiera para Cupos y Cuotas/ Recientes/Canceladas/a Comprar.
	* Realización de pruebas en certificación.

  @seccion_financiera @328
  Scenario: ocultar en la pantalla de entrada las variables correspondientes a la sección Financiera para Cupos y Cuotas/ Recientes/Canceladas/a Comprar
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de FINANCIERA
    Then And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cupos TDC y Rotativo Recientes en la sección Financiera
	And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cuotas Canceladas en la sección Financiera
	And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cupos TDC Cancelados en la sección Financiera
	And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cupos Rotativos Cancelados en la sección Financiera
	And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cuota Crédito a Comprar en la sección Financiera
	And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cuota Crédito Recién Aprobado en la sección Financiera
	And al seleccionar Cupos y cuotas /Recientes/ Canceladas/ a comprar igual a No, no se visualice el campo Cuota Crédito a comprar colilla en la sección Financiera
