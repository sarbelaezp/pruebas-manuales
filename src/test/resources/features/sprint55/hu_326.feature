#Author: sapalac@bancolombia.com.co
Feature: User Story 713217: DT2 - APM - HU - 326 Crear los nuevos campos en la pantalla de entrada, para las secciones: Solicitud, Financiera y Libranza Estándar
	Criterios de aceptación:
  * Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo, puntualmente, incluir los nuevos campos en la secciones: Solicitud, Financiera y Libranza Estándar.
	* Realización de pruebas en certificación.

  @seccion_solicitud @326
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de SOLICITUD
    Then visualiza los campos (Canal de Venta, Tipo Producto, Producto, Aplica Libranza, Valor Solicitado, Plazo Solicitado, Tasa, Compra de cartera/Retanqueo y Código de convenio) se muestren en la sección de Solicitud
		And se adicione el campo Aplica Libranza de tipo Radiobutton debajo de Producto
		And el campo Aplica Libranza sea obligatorio
		And se adicione el campo Plazo Solicitado de tipo texto debajo de Valor Solicitado
		And el campo Plazo Solicitado sea obligatorio
		And se adicione el campo Compra de cartera/Retanqueo de tipo Radiobutton debajo de Aplica Libranza
		And el campo Compra de cartera/Retanqueo sea opcional
		And se adicione el campo Código de convenio de tipo texto debajo de Compra de cartera y Retanqueo
		And el campo Código de convenio sea opcional
    
 
	@seccion_personal @326
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de PERSONAL
    Then visualiza los campos (Tipo Documento, Número Documento, Fecha de Nacimiento, Región, Segmento, Estado Civil, Personas a Cargo, Nivel Académico, Tipo Vivienda, Estrato y Género) se muestren en la sección de Personal
		And se visualice la información de personal en dos columnas
		And la primera columna contenga los campos: Tipo Documento, Número Documento, Fecha de Nacimiento, Región, Segmento
		And la segunda columna contenga los campos: Fecha de Nacimiento, Personas a Cargo, Nivel Académico, Tipo Vivienda, Estrato y Género
		And se adicione el campo Estrato debajo de Tipo Vivienda
		And el campo Estrato sea opcional de tipo Seleccionable

	@seccion_ocupacion @326
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de OCUPACION
    Then visualiza los campos (Ocupación, Tipo Contrato, Tiempo Actividad (meses), Tipo de Programa y Semestre) se muestren en la sección de Ocupación
		And se muestren en la primera columna los campos: Ocupación, Tipo Contrato, Tiempo Actividad (meses) y Tipo de Programa
		And se muestre en la segunda columna el campo Semestre 
		And se adicione el campo Semestre al frente de Ocupación
		And el campo Semestre sea opcional de tipo Seleccionable


	@seccion_financiera @326
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de FINANCIERA
    Then visualiza los campos (Ingresos Fijos (Salario fijo, mesada pensional), Ingresos Variables, Ingreso como Independiente, Otros ingresos/Rentas/Honorarios/Giros, Gasto Familiar, Tiene Bienes Raices, Valor Total de los Activos, Formato de conocimiento de cliente, Cupos y cuotas /Recientes/ Canceladas/ a comprar, Cupos TDC y Rotativo Recientes, Cuotas Canceladas, Cupos TDC Cancelados, Cupos Rotativos Cancelados, Cuota Crédito a Comprar, Cuota Crédito Recién Aprobado y Cuota Crédito a comprar colilla) en la sección de Financiera
		And no se visualicen en la pantalla de Crear Solicitud el campo Declarante
		And no se visualicen en la pantalla de Crear Solicitud el campo Ingreso Giros
		And no se visualicen en la pantalla de Crear Solicitud el campo Ingreso Renta y Honorarios
		And se adicione el campo Otros ingresos/Rentas/Honorarios/Giros en la sección Financiera
		And el campo Otros ingresos/Rentas/Honorarios/Giros en la sección Financiera sea opcional de tipo numérico
		And se adicione el campo Formato de conocimiento de cliente en la sección Financiera sea obligatorio de tipo radiobutton
		And se adicione el campo Cuota Crédito a comprar colilla en la sección Financiera sea obligatorio de tipo numérico


	@seccion_libranza_estandar @326
  Scenario: Iniciar con los desarrollos necesarios en APM para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo de Crear solicitud
    When la usuaria visualiza la sección de LIBRANZA ESTANDAR
    Then visualiza los campos (Tipo de Empresa, Calificación Interna Empresa-Convenio, Segmento Empresa-Convenio, Seguro de Desempleo, Seguro vida más, Fecha Vinculación Empresa, Deducciones Totales y Porcentaje Deducciones) en la sección de Libranza Estándar
		And se adicione el campo Tipo de Empresa de tipo Seleccionable
		And se adicione el campo Calificación Interna Empresa-Convenio de tipo Seleccionable
		And se adicione el campo Segmento Empresa-Convenio de tipo Seleccionable
		And se adicione el campo Seguro de Desempleo de tipo Radiobutton
		And se adicione el campo Seguro vida más de tipo Radiobutton
		And se adicione el campo Fecha Vinculación Empresa de tipo Calendario
		And se adicione el campo Deducciones Totales de tipo Numérico
		And se adicione el campo Porcentaje Deducciones de tipo Numérico
		And el campo Tipo de Empresa sea opcional
		And el campo Calificación Interna Empresa-Convenio sea opcional
		And el campo Segmento Empresa-Convenio sea opcional
		And el campo Seguro de Desempleo sea opcional
		And el campo Seguro vida más sea opcional
		And el campo Fecha Vinculación Empresa sea opcional
		And el campo Deducciones Totales sea opcional
		And el campo Porcentaje Deducciones sea opcional
