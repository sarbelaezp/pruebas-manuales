#Author: sapalac@bancolombia.com.co
Feature: User Story 761966: DT2 - APM - HU - 376 Realización de paso a producción de Shell de procesos: Captaciones, Junta Directiva y Score Comportamiento
	Criterios de aceptación:
  * Pasar los archivos Shell en el ambiente productivo que corresponden a las ETL's
  - SEQ_EJE_PPAL_RIESGOS_CAPTACIONES.sh
  - SEQ_EJE_PPAL_RIESGOS_JUNTA_DIR.sh
  - SEQ_EJE_PPAL_RIESGOS_SCORE.sh
  - SEQ_EJE_ROLLBACK_RIESGOS_CAPTACIONES.sh
  - SEQ_EJE_ROLLBACK_RIESGOS_JUNTA_DIR.sh
  - SEQ_EJE_ROLLBACK_RIESGOS_SCORE.sh

  @ETL_JUNTA @ETL
  Scenario: Pasar los archivos Shell en el ambiente productivo que corresponden a las ETL's
    Given se ingresa al aplicativo del Datastage
    When se vaya a ejecutar la carga de información desde la LZ a la BDRiesgos se ejecute el stored procedure asociado a la ETL de Junta directiva
    Then que se ejecute la ETL de Respaldo de Junta directiva
	And que se ejecute la ETL Principal de Junta directiva
	And que las ETL ejecutadas no queden abortadas, no compiladas o con advertencias
	
  @ETL_COMPORTAMIENTO @ETL
  Scenario: Pasar los archivos Shell en el ambiente productivo que corresponden a las ETL's
    Given se ingresa al aplicativo del Datastage
    When se vaya a ejecutar la carga de información desde la LZ a la BDRiesgos se ejecute el stored procedure asociado a la ETL de Score comportamiento
    Then que se ejecute la ETL de Respaldo de Score comportamiento
	And que se ejecute la ETL Principal de Score comportamiento
	And que las ETL ejecutadas no queden abortadas, no compiladas o con advertencias
	
	@ETL_CAPTACIONES @ETL
  Scenario: Pasar los archivos Shell en el ambiente productivo que corresponden a las ETL's
    Given se ingresa al aplicativo del Datastage
    When se vaya a ejecutar la carga de información desde la LZ a la BDRiesgos se ejecute el stored procedure asociado a la ETL de Captaciones
    Then que se ejecute la ETL de Respaldo de Captaciones
	And que se ejecute la ETL Principal de Captaciones
	And que las ETL ejecutadas no queden abortadas, no compiladas o con advertencias