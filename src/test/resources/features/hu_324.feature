#Author: sapalac@bancolombia.com.co
Feature: User Story 692903: DT2 - APM - HU - 324 Desarrollo de Trigger para realización de control para el área de Monitoreo
	Criterios de aceptación:
  * Implementación del Trigger.
  * Creación de tabla de consulta.

  @manual
  Scenario: Creación de tabla de consulta de usuarios con los campos indicados
    Given se crea la tabla GESTION_USUARIO_APM en Certificación
    When ingreso a la base de datos de APM
    Then que la tabla contenga el campo ID
    And que la tabla contenga el campo USER_ID
    And que la tabla contenga el campo USER_NAME
    And que la tabla contenga el campo USER_LOGIN
    And que la tabla contenga el campo APPLICATION_ID
    And que la tabla contenga el campo APPLICATION_NAME
    And que la tabla contenga el campo IS_APPLI_ACTIVE_USER
    And que la tabla contenga el campo CREATE_USER_ID
    And que la tabla contenga el campo CREATE_USER_NAME
    And que la tabla contenga el campo CREATE_USER_LOGIN
    And que la tabla contenga el campo UPDATE_DATE

  @manual
  Scenario: Creación de tabla de consulta de usuarios con la longitud esperada
    Given se crea la tabla GESTION_USUARIO_APM en Certificación
    When ingreso a la base de datos de APM
    Then que la tabla contenga el campo ID de tipo NUMBER (10, 0) NOT NULL
    And que la tabla contenga el campo USER_ID de tipo NUMBER (10, 0) NOT NULL
    And que la tabla contenga el campo USER_NAME de tipo VARCHAR2(256 CHAR) POR DEFECTO ‘ ‘
    And que la tabla contenga el campo USER_LOGIN de tipo VARCHAR2(256 CHAR) POR DEFECTO ‘ ‘
    And que la tabla contenga el campo APPLICATION_ID de tipo NUMBER (10, 0) NOT NULL
    And que la tabla contenga el campo APPLICATION_NAME de tipo VARCHAR2(50 CHAR) POR DEFECTO ‘ ‘
    And que la tabla contenga el campo IS_APPLI_ACTIVE_USER de tipo NUMBER (1, 0) NOT NULL
    And que la tabla contenga el campo CREATE_USER_ID de tipo NUMBER (10, 0) NOT NULL
    And que la tabla contenga el campo CREATE_USER_NAME de tipo VARCHAR2(256 CHAR) POR DEFECTO ‘ ‘
    And que la tabla contenga el campo CREATE_USER_LOGIN de tipo VARCHAR2(250 CHAR) POR DEFECTO ‘ ‘
    And que la tabla contenga el campo UPDATE_DATE de tipo TIMESTAMP (6) NOT NULL

  @manual
  Scenario: Implementación del Trigger al asignar permisos de OM APM BANCOLOMBIA
    Given exista el trigger GEST_USU_APM_AF_INS_UPD_TRIG en Certificación
    When se cree un usuario en el aplicativo de APM con permisos de OM APM BANCOLOMBIA
    Then se guarde un registro del usuario creado en la tabla GESTION_USUARIO_APM
    
  @manual
  Scenario: Implementación del Trigger al asignar permisos de OM APM SUFI
    Given exista el trigger GEST_USU_APM_AF_INS_UPD_TRIG en Certificación
    When se cree un usuario en el aplicativo de APM con permisos de OM APM SUFI
    Then se guarde un registro del usuario creado en la tabla GESTION_USUARIO_APM
    
  @manual
  Scenario: Implementación del Trigger al asignar permisos de OM APM VEHICULOS
    Given exista el trigger GEST_USU_APM_AF_INS_UPD_TRIG en Certificación
    When se cree un usuario en el aplicativo de APM con permisos de OM APM VEHICULOS
    Then se guarde un registro del usuario creado en la tabla GESTION_USUARIO_APM
    
    @manual
  Scenario: Implementación del Trigger al quitar permisos de OM APM BANCOLOMBIA
    Given exista el trigger GEST_USU_APM_AF_INS_UPD_TRIG en Certificación
    When se cree un usuario en el aplicativo de APM con permisos de OM APM BANCOLOMBIA
    Then se guarde un registro del usuario creado en la tabla GESTION_USUARIO_APM
    
  @manual
  Scenario: Implementación del Trigger al quitar permisos de OM APM SUFI
    Given exista el trigger GEST_USU_APM_AF_INS_UPD_TRIG en Certificación
    When se cree un usuario en el aplicativo de APM con permisos de OM APM SUFI
    Then se guarde un registro del usuario creado en la tabla GESTION_USUARIO_APM
    
  @manual
  Scenario: Implementación del Trigger al quitar permisos de OM APM VEHICULOS
    Given exista el trigger GEST_USU_APM_AF_INS_UPD_TRIG en Certificación
    When se quiten los permisos del usuario en el aplicativo de APM con permisos de OM APM VEHICULOS
    Then se guarde un registro del usuario con los permisos que tenga en la tabla GESTION_USUARIO_APM
    