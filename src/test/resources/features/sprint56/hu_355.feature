#Author: sapalac@bancolombia.com.co
Feature: User Story 740992: DT2 - APM - HU - 355 Creación de las nuevas variables con valores de dominio: Segmento Empresa Convenio
	Criterios de aceptación:
  * Incluir los nuevos dominios de las variables Segmento Empresa - Convenio para implementar las modificaciones en la restructuración 
  	de la pantalla de entrada de Consumo


  @PRODUCTO_PLAZO @Sprint56
  Scenario: Incluir los nuevos dominios de las variables Segmento Empresa - Convenio para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo Admin Sistema
    When se intente parametrizar valores para Segmento Empresa-Convenio
    Then que se permita parametrizar la lista Segmento Empresa-Convenio
	And que se visualice la opción CONSTRUCTOR CORPORATIVO
	And que se visualice la opción CONSTRUCTOR EMPRESARIAL
	And que se visualice la opción CONSTRUCTOR PYME
	And que se visualice la opción CORPORATIVO
	And que se visualice la opción EMPRESARIAL
	And que se visualice la opción INSTITUCIONES FINANCIERAS
	And que se visualice la opción GOBIERNO
	And que se visualice la opción GOBIERNO DE RED
	And que se visualice la opción PYMES


	
  @CALIFICACION_INTERNA_EMPRESA @Sprint56
  Scenario: Incluir los nuevos dominios de las variables Segmento Empresa - Convenio para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo Admin Sistema
    When se intente parametrizar valores para CALIFICACIÓN INTERNA EMPRESA
    Then que se permita parametrizar la lista Calificación Interna Empresa-Convenio
	And que se visualice la opción R1
	And que se visualice la opción R2
	And que se visualice la opción R3
	And que se visualice la opción R4
	And que se visualice la opción R5
	And que se visualice la opción R6
	And que se visualice la opción R7
	And que se visualice la opción R8
	And que se visualice la opción R9
	And que se visualice la opción R10
	And que se visualice la opción R11


	
	@SEMESTRE @Sprint56
  Scenario: Incluir los nuevos dominios de las variables Segmento Empresa - Convenio para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo Admin Sistema
    When se intente parametrizar valores para SEMESTRE
    Then que se permita parametrizar la lista Semestre
	And que se visualice la opción 1
	And que se visualice la opción 2
	And que se visualice la opción 3
	And que se visualice la opción 4
	And que se visualice la opción 5
	And que se visualice la opción 6
	And que se visualice la opción 7
	And que se visualice la opción 8
	And que se visualice la opción 9
	And que se visualice la opción 10
	And que se visualice la opción 11
	And que se visualice la opción 12

    
    
    @SEGMENTO_EMPRESA_CONVENIO @Sprint56
  Scenario: Incluir los nuevos dominios de las variables Segmento Empresa - Convenio para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo Admin Sistema
    When se intente parametrizar valores para Estrato
    Then que se permita parametrizar la lista Estrato
	And que se visualice la opción 1
	And que se visualice la opción 2
	And que se visualice la opción 3
	And que se visualice la opción 4
	And que se visualice la opción 5
	And que se visualice la opción 6

    
    
    @ESTRATO @Sprint56
  Scenario: Incluir los nuevos dominios de las variables Segmento Empresa - Convenio para implementar las modificaciones en la restructuración de la pantalla de entrada de Consumo
    Given se ingresa al aplicativo de APM Consumo en Certificación
    And se ingresa al módulo Admin Sistema
    When se intente parametrizar valores para Tipo de Empresa
    Then que se permita parametrizar la lista Tipo de Empresa
	And que se visualice la opción EMPRESA DE SERVICIOS TEMPORAL
	And que se visualice la opción EMPRESA DE SERVICIOS NO TEMPORAL

    