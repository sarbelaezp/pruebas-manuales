#Author: sapalac@bancolombia.com.co
Feature: User Story 699446: DT2 - CON - HU - 307 Separar los ADB de Motor según producto (SI y Consumo)
	Criterios de aceptación:
  * Accesos independientes para Consumo y SI.
	* Permitir abrir el repositorio en Explorer.
	* Visualización de la parametrización actual de Motor en los dos repositorios, según corresponda. 
	* Acompañamiento a la realización de pruebas de visualización y acceso al nuevo repositorio de Motor. Estas pruebas las realizará Estefania Morales de la Gerencia de Ecosistemas.
  

  @manual
  Scenario: Acompañamiento a pruebas de la usuaria en Motor
    Given se acompaña a la usuaria en las pruebas de Motor
    When la usuaria intenta parametrizar nuevas reglas en Motor
    Then se guardan las nuevas reglas
    And el funcionamiento de Motor continúa sin inconvenientes
 
  @manual
  Scenario: Permite abrir el repositorio en Explorer
    Given se crea el nuevo repositorit de Motor
    When se accede al aplicativo por Internet Explorer
    Then se permite el acceso al aplicativo de Motor
