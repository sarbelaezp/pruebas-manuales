#Author: sapalac@bancolombia.com.co
Feature: User Story 697321: DT2 - APM - HU - 321 Nuevos campos de la tabla de Score de Comportamiento y un campo de preaprobados
	Criterios de aceptación:
  * Que viajen los campos en el flujo de la BD_Riesgos
  * Que no se afecte el funcionamiento del aplicativo
  

  @manual
  Scenario: Mapeo del campo PD en APM Consumo
    Given se asigna un valor diferente a 0 al campos PD en la tabla GMRC_SCORE_COMPORTAMIENTO de la BD Riesgos
    And se ingresa al aplicativo de APM Consumo en Certificación
    When se crea una solicitud en APM Consumo
    Then se envia el campo PD en el request de OMDM_APR en la variable CAMPO_USUARIO_312
 
  @manual
  Scenario: Mapeo del campo regla en APM Consumo
    Given se asigna un valor diferente a 0 al campos regla en la tabla GMRC_SCORE_COMPORTAMIENTO de la BD Riesgos
    And se ingresa al aplicativo de APM Consumo en Certificación
    When se crea una solicitud en APM Consumo
    Then se envia el campo regla en el request de OMDM_APR en la variable CAMPO_USUARIO_291
 
  @manual
  Scenario: Mapeo del campo grp_riesgo en APM Consumo
    Given se asigna un valor diferente a 0 al campos grp_riesgo en la tabla PRECALCULADO de la BD Riesgos
    And se ingresa al aplicativo de APM Consumo en Certificación
    When se crea una solicitud en APM Consumo
    Then se envia el campo grp_riesgo en el request de OMDM_APR en la variable CAMPO_USUARIO_292
 