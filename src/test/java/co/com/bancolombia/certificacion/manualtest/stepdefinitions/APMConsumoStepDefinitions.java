package co.com.bancolombia.certificacion.manualtest.stepdefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

import static co.com.bancolombia.certification.manualtestlib.ManualTest.validar;

public class APMConsumoStepDefinitions {
    private Scenario escenario;

    @Before
    public void getScenario(Scenario escenario) {
        this.escenario = escenario;
    }

    @Given("^(.*)$")
    public void casoCorrido(String paso){
        validar(paso, escenario.getName());
    }
}
