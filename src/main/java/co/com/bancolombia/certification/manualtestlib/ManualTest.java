package co.com.bancolombia.certification.manualtestlib;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import static org.junit.Assert.assertEquals;

public class ManualTest {
    public static void validar(String paso, String nameScenario) {
        int SI = 1;
        String reason = "";
        String[] options = {"No", "Si"};
        int optionSelected = JOptionPane.showOptionDialog(new JFrame(), "El paso \"" + paso + "\" fue ejecutado correctamente?",
                nameScenario, JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                null, options, options[0]);
        if(optionSelected == 0) {
            reason = JOptionPane.showInputDialog("Ingrese la raz�n por la que el caso no corrió correctamente");
        }
        assertEquals(reason, SI, optionSelected);
    }
}
